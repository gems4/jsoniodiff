#include <iostream>
#include <fstream>
#include "jsonioDiff/diffkeyvalue.h"

using namespace std;

int main(int argc, char* argv[])
{
    // Template file name in a directory to compare
    std::string file_name_templ= "";//"template_diff.json";

    // Input file
    std::string templ_path = "reac1/pHtitr-ipm.dat";

    // Result file
    std::string new_path = "reac1-json/pHtitr-ipm.json";

    try{
        if ( argc >  1)
            templ_path = argv[1];

        if ( argc >  2)
            new_path = argv[2];

        if ( argc >  3)
            file_name_templ = argv[3];

        // KeyValueFile template to reading key-value template file structure
        jsoniodiff::KeyValueFile templ_file(templ_path, file_name_templ);

        if( !templ_file.exist() )
        {
          std::cout <<  "File : " <<  templ_path << " not exists \n";
          return 1;
        }

        templ_file.load_all();

        ofstream fout(new_path);
        fout << templ_file.json_string() << std::endl;

    }
    catch(std::exception& e)
    {
        std::cerr <<   "std::exception: " << e.what() <<  std::endl;
    }
    catch(...)
    {
       std::cerr <<  "unknown exception" <<  std::endl;
    }
    return 0;
}
