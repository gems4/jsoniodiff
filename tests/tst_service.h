#pragma once

#include <gtest/gtest.h>

#include "jsonioDiff/detail.h"
#include "jsonioDiff/txtfiles.h"

using namespace testing;
using namespace jsoniodiff;

TEST( JsDetail, Trim )
{
    std::string str{" \t\rTest \n "};
    trim( str );
    EXPECT_EQ( "Test", str );
    str = ":: Test2 ;";
    trim( str, ": ;" );
    EXPECT_EQ( "Test2", str );
}

//  Function that can be used to split text using regexp
TEST( JsDetail, regexpSplit )
{
    std::string key_temp = "<([^>]*)>";
    auto tokens = regexp_split( "abs <k1>1\r 22 333 \n<k2> aaa", key_temp );
    // std::cout << dump(tokens) << std::endl;
    EXPECT_EQ( tokens.size(), 3 );
    EXPECT_EQ( "1\r 22 333", tokens[1] );
    auto strquery = regexp_split("a:bb:ccc", ":" );
    EXPECT_EQ( "aaa", tokens[2] );
}

//  Function that can be used to extract tokens using regexp
TEST( JsDetail, regexpExtract )
{

    std::string key_temp = "<([^>]*)>";
    auto tokens = regexp_extract( "abs <k1>1\r 22 333 \n<k2> aaa", key_temp );
    // std::cout << dump(tokens) << std::endl;
    EXPECT_EQ( tokens.size(), 2 );
    EXPECT_EQ( "<k1>", tokens[0] );
    auto strquery = regexp_split("a:bb:ccc", ":" );
    EXPECT_EQ( "<k2>", tokens[1] );


}

//  Function that can be used to replase text using regexp
TEST( JsDetail, regexpReplace )
{
    std::string com_rexp = "#([^\\\n]*)\\\n";
    auto resstr = regexp_replace("# comment 1 \n<k1> 34\n#comment 2\n" ,com_rexp, "");
    EXPECT_EQ( "<k1> 34\n", resstr );
}



// Test filesystem ----------------------------------------------------------------------

#include <fstream>
#include <filesystem>
namespace fs = std::filesystem;


TEST( JsDetail, TestDir )
{
    std::string test_dir = "test_dir";
#ifndef _WIN32
    if( path_exist( test_dir ) )
        fs::remove_all(test_dir);
#endif
    fs::create_directory(test_dir);
    std::ofstream(test_dir+"/file1.txt").put('a');
    std::ofstream(test_dir+"/file2.txt").put('b');
    std::ofstream(test_dir+"/file3.json").put('1');

    auto files = files_into_directory( test_dir, "txt");
    EXPECT_EQ( files.size(), 2);
#ifdef _WIN32
    EXPECT_TRUE(std::find(files.begin(), files.end(),test_dir+"\\file1.txt")!=files.end() );
    EXPECT_TRUE(std::find(files.begin(), files.end(),test_dir+"\\file2.txt")!=files.end() );
#else
    EXPECT_TRUE(std::find(files.begin(), files.end(),test_dir+"/file1.txt")!=files.end() );
    EXPECT_TRUE(std::find(files.begin(), files.end(),test_dir+"/file2.txt")!=files.end() );
#endif
}

