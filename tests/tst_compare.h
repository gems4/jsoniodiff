﻿#pragma once

#include <gtest/gtest.h>

#include <list>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <forward_list>

#include "jsonioDiff/compare.h"

using namespace testing;
using namespace jsoniodiff;

TEST( JsCompare, compareInt )
{
    Comparator comp;

    EXPECT_TRUE( comp.compare( 1,1 ) );
    EXPECT_TRUE( comp.compare<std::string>( "1","1 " ) );

    EXPECT_FALSE( comp.compare( 1, 2 ) );
    EXPECT_EQ( comp.getDifference(), "1                    2                   ");
    EXPECT_FALSE( comp.compare<std::string>( "1","2 " ) );
}

TEST( JsCompare, compareFloat )
{
    Comparator comp;

    EXPECT_FALSE( comp.compare( 1e-12, 0.1e-12 ) );
    EXPECT_EQ( comp.getDifference(), "1e-12                1e-13               ");

    EXPECT_FALSE( comp.compare<std::string>( "1.e-3",".002" ) );
    EXPECT_EQ( comp.getDifference(), "1.e-3                .002                ");

    EXPECT_TRUE( comp.compare( 10e-12, 1e-11 ) );
    EXPECT_TRUE( comp.compare<std::string>( "1.e-3",".001" ) );
    EXPECT_EQ( comp.getDifference(), "");

    comp.setEpsilon( 0.05, FloatCompareMethod::AbsDiffMore );

    EXPECT_FALSE( comp.compare( 100., 95.1 ) );

    comp.setEpsilon( 0.05, FloatCompareMethod::AprDiffMore );

    EXPECT_TRUE( comp.compare( 100., 95.1 ) );

    comp.setEpsilon( 0.05, FloatCompareMethod::RelDiffMore );

    EXPECT_FALSE( comp.compare( 100., 95.1 ) );
}

TEST( JsCompare, compareString )
{
    Comparator comp;

    EXPECT_TRUE( comp.compare<std::string>( "a bb ccc ","a\tbb\tccc" ) );
    EXPECT_EQ( comp.getDifference(), "");
    EXPECT_FALSE( comp.compare<std::string>( "a bb ccc ","a dd ccc" ) );
    EXPECT_EQ( comp.getDifference(), "\n          1        bb                   dd                  ");

    EXPECT_FALSE( comp.compare<std::string>( "a bb ccc ","a bb" ) );
    EXPECT_EQ( comp.getDifference(), "The number of values is different: left = 3 right = 2");

    EXPECT_FALSE( comp.compare<std::string>( "a bb ","a dd bb ccc" ) );
    EXPECT_EQ( comp.getDifference(), "The number of values is different: left = 2 right = 4");

    comp.setEpsilon( 0.05, FloatCompareMethod::AbsDiffMore );
    EXPECT_TRUE( comp.compare<std::string>( "abc 1 1e3  1.5","abc 1 1000 1.54" ) );
    EXPECT_EQ( comp.getDifference(), "");
}


TEST( JsCompare, compareVector )
{
    Comparator comp;

    std::vector<int> left{ 1, 2, 3, 4 };
    std::vector<int> right{ 1, 2, 3, 4 };

    EXPECT_TRUE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "");

    right[2] = 5;
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "\n          2        3                    5                   ");

    left.resize(2);
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "The number of values is different: left = 2 right = 4");
    comp.setMethod(Comparator::ConsistIn);
    EXPECT_TRUE( comp.compare( left, right ) );

    right.resize(1);
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "The number of values is different: left = 2 right = 1");
}
TEST( JsCompare, compareSet )
{
    Comparator comp;

    std::set<int> left{ 1, 2, 3, 4 };
    std::set<int> right{ 1, 2, 3, 4 };

    EXPECT_TRUE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "");

    std::list<int> lleft{ 1, 2, 3, 4 };
    std::list<int> lright{ 1, 2, 3, 4 };

    EXPECT_TRUE( comp.compare( lleft, lright ) );
    EXPECT_EQ( comp.getDifference(), "");
}



TEST( JsCompare, compareMap )
{
    Comparator comp;

    std::map<std::string,int> left{ {"a",1}, {"b",2}, {"c",3}, {"d",4} };
    std::map<std::string,int> right{ {"a",1}, {"b",2}, {"c",3}, {"d",4} };

    EXPECT_TRUE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "");

    right["c"] = 5;
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "\nc                  3                    5                   ");

    left.erase("c");
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "\nPresent only in right: c");
    comp.setMethod(Comparator::ConsistIn);
    EXPECT_TRUE( comp.compare( left, right ) );

    right.erase("d");
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "\nPresent only in left: d");
    comp.setMethod(Comparator::Difference);
    EXPECT_FALSE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "\nPresent only in left: d\nPresent only in right: c");
}

TEST( JsCompare, compareOtherMap )
{
    Comparator comp;

    std::unordered_map<std::string,int> left{ {"a",1}, {"b",2}, {"c",3}, {"d",4} };
    std::unordered_map<std::string,int> right{ {"a",1}, {"b",2}, {"c",3}, {"d",4} };

    EXPECT_TRUE( comp.compare( left, right ) );
    EXPECT_EQ( comp.getDifference(), "");
}


//----------------------------------------------------------

double NormDoubleRound(double aVal, int digits)
{
    double val;
    std::stringstream sbuf;
    sbuf << std::setprecision(digits) << aVal;
    sbuf >> val;
    return val;
}

float NormFloatRound(float aVal, int digits)
{
    float val;
    std::stringstream sbuf;
    sbuf << std::setprecision(digits) << aVal;
    sbuf >> val;
    return val;
}

double NormDoubleRoundOld(double aVal, int digits)
{
    double val;
    char vbuf[30];	// double is ~15 digit   PATTERN_GET()
    sprintf(vbuf, "%.*le" , digits , aVal);
    sscanf(vbuf, "%le", &val );
    return val;
}

float NormFloatRoundOld(float aVal, int digits)
{
    float val;
    char vbuf[30];	// double is ~15 digit   PATTERN_GET()
    sprintf(vbuf, "%.*e" , digits, aVal);
    sscanf(vbuf, "%e", &val );
    return val;
}

static double dtond( const double d, const int n = 7)
{
    if (d == 0.0 || n > 13 || n < 2)
        return d;
    double f = pow(10.0, n - ceil(log10(fabs(d))));
    return round(d * f) / f;
}

TEST( GEMSCompare, NormDoubleRound )
{
    //std::cout << std::setprecision(16) <<  NormDoubleRound(1.12345678, 3) << std::endl;
    EXPECT_EQ( NormDoubleRound(1./3., 5), NormDoubleRound(1./3., 5) );
    EXPECT_EQ( NormDoubleRound(1./3., 5), 0.33333 );
    EXPECT_NE( NormDoubleRound(1./3., 5), NormDoubleRound(1./3., 6) );
    EXPECT_EQ( NormDoubleRound(1./3., 6), NormDoubleRoundOld(1./3., 5) );
    EXPECT_EQ( NormDoubleRound(1./3., 4), NormDoubleRoundOld(1./3., 3) );

    EXPECT_EQ( NormDoubleRound(1./6., 5), NormDoubleRound(1./6., 5) );
    EXPECT_EQ( NormDoubleRound(1./6., 5), 0.16667 );
    EXPECT_NE( NormDoubleRound(1./6., 5), NormDoubleRound(1./6., 6) );
    EXPECT_EQ( NormDoubleRound(1./6., 6), NormDoubleRoundOld(1./6., 5) );
    EXPECT_EQ( NormDoubleRound(1./6., 4), NormDoubleRoundOld(1./6., 3) );

    EXPECT_EQ( NormDoubleRound(1.12345678, 6), NormDoubleRoundOld(1.12345678, 5) );
    EXPECT_EQ( NormDoubleRound(1.12345678, 4), NormDoubleRoundOld(1.12345678, 3) );

    // 1.1235 1.1235
    //std::cout << std::setprecision(16) <<  NormDoubleRound(1.12345678, 5) << " " <<
    //             std::setprecision(16) <<  NormDoubleRoundOld(1.12345678, 4) << std::endl;
    // -1.797693134862316e+308 -inf
    //std::cout << std::setprecision(16) <<  NormDoubleRound(std::numeric_limits<double>::lowest(), 3) << " " <<
    //             std::setprecision(16) <<  NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 2 ) << std::endl;
    // -1.79769313e+308 -1.79769313e+308
    //std::cout << std::setprecision(16) <<  NormDoubleRound(std::numeric_limits<double>::lowest(), 9) << " " <<
    //             std::setprecision(16) <<  NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 8) << std::endl;

    EXPECT_EQ( NormDoubleRound(std::numeric_limits<double>::lowest(), 6), NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 5) );
    // -1.79769e+308 and -inf
    EXPECT_NE( NormDoubleRound(std::numeric_limits<double>::lowest(), 3), NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 3) );

    EXPECT_EQ( NormDoubleRound(std::numeric_limits<double>::max(), 6), NormDoubleRoundOld(std::numeric_limits<double>::max(), 5) );
    // 1.79769e+308 and inf
    EXPECT_NE( NormDoubleRound(std::numeric_limits<double>::max(), 4), NormDoubleRoundOld(std::numeric_limits<double>::max(), 3) );

    EXPECT_EQ( NormDoubleRound(std::numeric_limits<double>::min(), 13), NormDoubleRoundOld(std::numeric_limits<double>::min(), 12) );
    EXPECT_EQ( NormDoubleRound(std::numeric_limits<double>::min(), 17), NormDoubleRoundOld(std::numeric_limits<double>::min(), 16) );
    EXPECT_EQ( NormDoubleRound(std::numeric_limits<double>::min(), 2), NormDoubleRoundOld(std::numeric_limits<double>::min(), 1) );
}

TEST( GEMSCompare, dtond )
{
    //std::cout << std::setprecision(16) <<  NormDoubleRound(1.12345678, 3) << std::endl;
    EXPECT_EQ( dtond(1./3., 5), dtond(1./3., 5) );
    EXPECT_EQ( dtond(1./3., 5), 0.33333 );
    EXPECT_NE( dtond(1./3., 5), dtond(1./3., 6) );
    EXPECT_EQ( NormDoubleRound(1./3., 6), NormDoubleRoundOld(1./3., 5) );
    EXPECT_EQ( NormDoubleRound(1./3., 4), NormDoubleRoundOld(1./3., 3) );

    EXPECT_EQ( dtond(1./6., 5), dtond(1./6., 5) );
    EXPECT_EQ( dtond(1./6., 5), 0.16667 );
    EXPECT_NE( dtond(1./6., 5), dtond(1./6., 6) );
    EXPECT_EQ( dtond(1./6., 6), NormDoubleRoundOld(1./6., 5) );
    EXPECT_EQ( dtond(1./6., 4), NormDoubleRoundOld(1./6., 3) );

    EXPECT_EQ( dtond(1.12345678, 6), NormDoubleRoundOld(1.12345678, 5) );
    EXPECT_EQ( dtond(1.12345678, 4), NormDoubleRoundOld(1.12345678, 3) );

    EXPECT_EQ( dtond(std::numeric_limits<double>::lowest(), 6), NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 5) );
    EXPECT_EQ( dtond(std::numeric_limits<double>::lowest(), 4), NormDoubleRoundOld(std::numeric_limits<double>::lowest(), 3) );

    EXPECT_EQ( dtond(std::numeric_limits<double>::max(), 6), NormDoubleRoundOld(std::numeric_limits<double>::max(), 5) );
    // inf and inf
    EXPECT_EQ( dtond(std::numeric_limits<double>::max(), 4), NormDoubleRoundOld(std::numeric_limits<double>::max(), 3) );

    // -nan and 2.22507e-308
    EXPECT_NE( dtond(std::numeric_limits<double>::min(), 13), NormDoubleRoundOld(std::numeric_limits<double>::min(), 12) );
    EXPECT_EQ( dtond(std::numeric_limits<double>::min(), 17), NormDoubleRoundOld(std::numeric_limits<double>::min(), 16) );
    // -nan and 2.2e-308
    EXPECT_NE( dtond(std::numeric_limits<double>::min(), 2), NormDoubleRoundOld(std::numeric_limits<double>::min(), 1) );
}
