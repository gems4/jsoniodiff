#include "jsonioDiff/compare.h"
#include "jsonioDiff/detail.h"


namespace jsoniodiff {


std::string Comparator::templ_name = "left";
std::string Comparator::source_name = "right";


std::string Comparator::size_diff_string(size_t lval_size, size_t rval_size) const
{
    // Think about recursion arrays => use yaml?
    std::ostringstream difss;
    difss <<  "The number of values is different: " <<
            templ_name << " = " << lval_size << " " << source_name << " = " << rval_size;
    return difss.str();
}

std::string Comparator::keys_diff_string(bool is_left, std::set<std::string> keys) const
{
    // Think about recursion arrays => use yaml?
    std::ostringstream difss;
    difss << "\nPresent only in ";
    if( is_left )
        difss << templ_name << ":";
    else
        difss << source_name << ":";

    for( const auto& el: keys )
        difss << " " << el;
    return difss.str();
}

bool Comparator::diff_simple(const std::string &lval, const std::string &rval, std::ostream &out)
{
    bool ret = false;

    if(  lval != rval  )
    {
        double ldbl, rdbl;
        ret = true;

        if( is( ldbl, lval ) && is( rdbl, rval ) )
        {
            std::ostringstream iss;
            ret = diff(ldbl, rdbl, iss );
        }
        if( ret )
            out << value_diff_string( lval, rval );
    }

    return ret;
}


bool Comparator::diff(const std::string &lval, const std::string &rval, std::ostream &out)
{
    if(  lval == rval  )
        return false;

    auto lval_list = regexp_split(lval);
    auto rval_list = regexp_split(rval);

    if( lval_list.size() == 1  || rval_list.size() == 1 )
    {
        return diff_simple( lval_list[0], rval_list[0], out );
    }
    else
    {
        // output string as array
        return diff( lval_list, rval_list, out );
    }

}


} // namespace jsoniodiff
