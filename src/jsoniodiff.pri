
HEADERS += \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/detail.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/txtfiles.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/diffkeyvalue.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/diffjson.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/diffkvasjson.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/compare.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/performer.h \
    $$JSONIODIFF_HEADERS_DIR/jsonioDiff/type_test.h

SOURCES += \
    $$JSONIODIFF_DIR/detail.cpp \
    $$JSONIODIFF_DIR/txtfiles.cpp \
    $$JSONIODIFF_DIR/diffkeyvalue.cpp \
    $$JSONIODIFF_DIR/diffjson.cpp \
    $$JSONIODIFF_DIR/diffkvasjson.cpp \
    $$JSONIODIFF_DIR/compare.cpp \
    $$JSONIODIFF_DIR/performer.cpp



