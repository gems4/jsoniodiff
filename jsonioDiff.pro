TEMPLATE = app

CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

!win32 {
  DEFINES += __unix
}

macx-g++ {
  DEFINES += __APPLE__
}

macx-clang {
  DEFINES += __APPLE__
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
}

win32 {
  INCLUDEPATH   += "C:\usr\local\include"
  DEPENDPATH   += "C:\usr\local\include"
  LIBPATH += "C:\usr\local\lib"
}

# Define the directory where jsonioDiff source code is located
JSONIODIFF_DIR =  $$PWD/src
JSONIODIFF_HEADERS_DIR =  $$JSONIODIFF_DIR/../include

DEPENDPATH   += $$JSONIODIFF_DIR
DEPENDPATH   += $$JSONIODIFF_HEADERS_DIR

INCLUDEPATH   += $$JSONIODIFF_DIR
INCLUDEPATH   += $$JSONIODIFF_HEADERS_DIR

OBJECTS_DIR   = obj


include($$JSONIODIFF_DIR/jsoniodiff.pri)

SOURCES += \
#        examples/kv_to_json.cpp
        main.cpp

DISTFILES += \
    doc/template_diff.json \
    doc/template_diff.md \
    doc/to_do.md
