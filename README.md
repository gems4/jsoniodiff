## JSONIODIFF

*JSONIODIFF* the command analyzes two key-value files or json files or dirs with such files and prints the difference.


##  Why use JSONIODIFF?

1. The order of data fields in the document is arbitrary (but in array fields the order of values is fixed)

2. Comparing floating point numbers choosing epsilon depends on the context, and determines how equal you want the numbers to be.

3. Any string split by space symbols and compare as string array. If both strings contain numbers, then we compare two double values.


## Application

```
Usage: jsonioDiff [ option(s) ] PATH_TEMPLATE PATH_SOURCE
Options:
        -h,        --help                show this help message
        -f,        --files               compare two files (default)
        -d,        --directories 		 compare all files in directories
        -r,        --recursive   		 recursively compare any subdirectories found
        -t,        --file-pattern PAT  	 only files that match PAT

        -k,        --key-value   		    compare key-value files
        -j,        --json   		        compare json  files
        -rd,       --rules-document JSONF   structured document describing data to compare

        -a,        --aproximately EPS	     approximately equal for floating types
        -e,        --essentially EPS	     essentially equal for floating types
        -at,       --absolute-tolerance EPS  absolute tolerance equal for floating types(default)

```


