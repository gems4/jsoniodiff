#include <iostream>
#include "jsonioDiff/performer.h"

using namespace std;

// -f -rd  dbr_diff.json -a  0.1e-6  -j "tst_inf/pHtitr-dbr-0-0000.json"  -j tst_inf/"pHtitr-dbr-0-1.json"
// -f -rd  dbr_diff.json -a  0.1e-6  -k "tst_inf/pHtitr-dbr-0-0000.dat"  -k "tst_inf/pHtitr-dbr-0-1.dat"

// -f -rd  dbr_diff.json -a  0.1e-6  -j "test_dir/pHtitr-dbr-0-0000.json"  -j "test_dir/Calculated-dbr.json"
// -d -r -t ".*-dbr-[\d-]*\.dat" -rd  dbr_diff.json -a  0.1e-6  -k Reactoro/v36 -k Reactoro/reac

// -f -rd  dbr_diff.json -a  0.1e-6  -j example4/example4-dbr-0-0000-gems3k.json  -j example4/example4-dbr-0-0000-rkt.json
// -f  -a  0.1e-6  -k test_dir/pHtitr-dbr-0-0000.dat  -k test_dir/Calculated-dbr.dat

int main(int argc, char* argv[])
{
    try{
        jsoniodiff::ComparisonPerformer performer(argc, argv);
        return performer.execute_command();
    }
    catch(std::exception& e)
    {
        std::cerr <<   "std::exception: " << e.what() <<  std::endl;
    }
    catch(...)
    {
       std::cerr <<  "unknown exception" <<  std::endl;
    }
    return 0;
}
