
TEMPLATE = app

CONFIG += console c++17
CONFIG += thread
CONFIG -= app_bundle
CONFIG -= qt

!win32 {
  DEFINES += __unix
}

macx-g++ {
  DEFINES += __APPLE__
}

macx-clang {
  DEFINES += __APPLE__
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
}

win32 {
  INCLUDEPATH   += "C:\usr\local\include"
  DEPENDPATH   += "C:\usr\local\include"
  LIBPATH += "C:\usr\local\lib"
}

# Define the directory where jsonioDiff source code is located
JSONIODIFF_DIR =  $$PWD/src
JSONIODIFF_HEADERS_DIR =  $$JSONIODIFF_DIR/../include
TESTS_DIR =  $$PWD/tests

DEPENDPATH   += $$JSONIODIFF_DIR
DEPENDPATH   += $$JSONIODIFF_HEADERS_DIR
DEPENDPATH   += $$TESTS_DIR

INCLUDEPATH   += $$JSONIODIFF_DIR
INCLUDEPATH   += $$JSONIODIFF_HEADERS_DIR
INCLUDEPATH   += $$TESTS_DIR


OBJECTS_DIR   = obj

include($$TESTS_DIR/gtest_dependency.pri)
include($$JSONIODIFF_DIR/jsoniodiff.pri)

HEADERS += \
        $$TESTS_DIR/tst_service.h \
        $$TESTS_DIR/tst_compare.h   \

SOURCES += \
        $$TESTS_DIR/main.cpp

