## Future implementation

!! KeyFile public froom json file

!!! Selection into performer key-value or json (3)


1. Possible output Comparator difference to yaml format  ( or use both )

2. Implement split string not only is_space but full string inro "" (regexpr take a lot of time)

3. class ComparisonPerformer for different file types, such fabric


```sh
jsonioDiff -h
jsonioDiff -d -r -rd  dbr_diff.json  -k Reactoro/v36 -k Reactoro/reac



jsonioDiff -f -rd  template_diff.json  v36/pHtitr-dbr-0-0000.dat reac1/pHtitr-dbr-0-0000.dat
jsonioDiff -d  -r -t ".*\.dat" -a  0.1e-6  "v36" "reac1"
jsonioDiff -d  -r -t ".*-dbr-[\d-]*\.dat" -a  0.1e-6  "v36" "reac1"
 -f  "v36-json/pHtitr-dbr-0-0000.json" "reac1-json/pHtitr-dbr-0-0000.json"


-f -rd  dbr_diff.json  Reactoro/v36/Kaolinite/SIA/pHtitrKa-dbr-0-0000.dat Reactoro/reac/Kaolinite/SIA/pHtitrKa-dbr-0-0000.dat
-f -rd  dbr_diff.json  Reactoro/v36/Kaolinite/SIA/pHtitrKa-dbr-0-0000.dat Reactoro/reac/Kaolinite/SIA/server_data/toServer-dbr-0-0000.dat
-f -rd  multi_after_diff.json  Reactoro/v36/Kaolinite/SIA/v36_after.dump.txt Reactoro/reac/Kaolinite/SIA/React_after.dump.txt
-f -rd  multi_after_diff_2.json  Reactoro/v36/Kaolinite/SIA/v36_after.dump.txt Reactoro/reac/Kaolinite/SIA/Reactoro_after.dump.txt
-f -rd  multi_before_diff.json  Reactoro/v36/Kaolinite/SIA/v36_before.dump.txt Reactoro/reac/Kaolinite/SIA/Reactoro_before.dump.txt
```


Now we get the same data into trunk and Reactore after calc with Simplex,
exept work arrays and G[]+F[] (this arrays different after calc and ExustatExpand )

Next steps:

0. Log time ( reseach 8h difference IPM Gui&standalone, 4h fix problem, 8h make compare scripts and test kaolinite AIA)
1. Test SIA
2. Hide work arrays on GUI
3. Test CalDolCol2 (AIA and SIA)
4. Test solvus (AIA and SIA)
5. Test all Dirs
6. Clear unnesary into calc init
7. run all tests
